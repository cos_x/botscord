/// Fonctionnalités qui consistent à envoyer des messages quand on ne lui demande rien.

use super::nvm;

/// Renvoie une phrase du type "J'aime le bruit blanc de l'eau".
/// Contenu aléatoire, basé sur la configuration actuelle.
pub fn create_aime() -> String
{
    let verb = nvm::get_random_like_verb();
    let liked = nvm::get_random_liked();
    return format!("{} {}.",verb, liked);
}

pub fn create_proverb() -> String
{
    return nvm::get_random_proverb();
}
