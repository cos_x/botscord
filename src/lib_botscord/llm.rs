use reqwest;
use log::{error, debug};
use serde_json;


#[derive(serde::Serialize)]
struct Message
{
    role : String,
    content : String
}

#[derive(serde::Serialize)]
struct PromptJson
{
    model : String,
    messages : Vec<Message>
}

/// Envoie par HTTP une requête au LLM de l'URL fournie.
///
/// # Arguments
///
/// * `system` - Prompt pour le système ("ex : you are a helpful assistant.")
/// * `prompt` - Le vrai prompt, auquel doit répondre le LLM
/// * `token` - Token d'accès au serveur du LLM (peut valoir n'importe quoi en test local avec ollama)
/// * `url` - URL du serveur du LLM (avec le "http(s)://")
pub fn request_llm_http(system : &str, prompt : &str, token : &str, url : &str) -> String
{
    let data = PromptJson
    {
        model : String::from("mistral"),
        messages : vec!
        [
            Message
            {
                role : String::from("system"),
                content : system.to_owned()
            },
            Message
            {
                role : String::from("user"),
                content : prompt.to_owned()
            }
        ]
    };
    let mut answer = String::from("Erreur interne !");
    if let Ok(data_json_string) = serde_json::to_string_pretty(&data)
    {

        let client = reqwest::blocking::Client::new();
        debug!("Sending HTTP post :\n{}", &data_json_string);
        if let Ok(res) = client.post(url)
        .header("Content-Type", "application/json")
        .header("api-key", token)
        .body(data_json_string).send()
        {
            if let Ok(text_response) = &res.text()
            {
                let result : Result<serde_json::Value, _> = serde_json::from_str(&text_response);
                if let Ok(json_response) = result
                {
                    if let Some(content) = json_response["choices"][0]["message"]["content"].as_str()
                    {
                        answer = String::from(content);
                    }
                    else
                    {
                        error!("No 'content' field in JSON answer :\n{}", text_response);
                    }
                }
                else
                {
                    error!("HTTP response is not a valid JSON :\n{}", text_response);
                }
            }
            else
            {
                error!("HTTP response is not valid text");
            }
        }
        else
        {
            error!("Cannot send HTTP POST request");
        }
    }
    else
    {
        error!("Cannot generate HTTP request JSON data");
    }
    return answer;
}
