use serenity::model::channel::Message;
use serenity::prelude::Context;
use log::debug;
use rand::{self, Rng};

use super::command_handler::handle_command;
use super::nvm;
use super::interventions;

/// Répartit le message reçu là où il doit être traité (selon si c'est une commande etc.).
///
/// # Arguments
///
/// * `ctx` - Contexte récupéré de [serenity]
/// * `msg` - Le message reçu (fromat "[serenity]")
///
/// # Returns
///
/// `Some(<réponse>)` si il y a un message à envoyer en réponse, ou `None`
pub fn dispatch_message(ctx: &Context, msg: &Message) -> Option<String>
{
    if msg.author != **ctx.cache.current_user()
    {
        debug!("Received message : {}", msg.content);
        if ! msg.content.is_empty() && msg.content.as_bytes()[0] == nvm::get_prefix() as u8
        {
            return handle_command(msg);
        }
        else
        {
            return random_intervention();
        }
    }
    return None;
}


/// Aléatoirement selon les probabilités configurées, déclenche une
/// intervention du bot et renvoie le message à envoyer.
/// Si une intervention se déclenche elle empêche les autres d'avoir lieu,
/// donc celle du début de la fonctions réduisent la probabilité effective
/// de celles de la fin.
///
/// # Returns
///
/// `Some(<message>)` si une intervention est déclenchée, ` None` sinon
fn random_intervention() -> Option<String>
{
    if rand::thread_rng().gen_bool(nvm::get_jaime_frequency() as f64)
    {
        debug!("Random intervention triggered : JAIME");
        return Some(interventions::create_aime());
    }
    if rand::thread_rng().gen_bool(nvm::get_proverb_frequency() as f64)
    {
        debug!("Random intervention triggered : proverb");
        return Some(interventions::create_proverb());
    }
    return None;
}
