/// NVM = Non Volatile Memory
/// Enregistrement de la config et autres données persistentes.

use std::{env, fs, path::Path, process::exit, sync::Mutex};
use serde::{self, Deserialize};
use serde_json;
use log::{debug, error, warn};
use rand;
use rand::seq::SliceRandom;
use lazy_static;

/* ==================== Types stockés par NVM ==================== */
/// Configuration générale pour le bot
#[derive(serde::Serialize, Deserialize)]
#[serde(default = "Configuration::default")]
struct Configuration
{
    /// Le préfixe pour toutes les commandes
    prefix : char,
    /// Fréquence (par message reçu) à laquelle le bot répondra par un "J'aime ..."
    jaime_frequency : f32,
    /// Fréquence (par message reçu) à laquelle le bot répond par un proverbe au hasard
    proverb_frequency : f32
}

/// Données relatives à la commande `aime`
#[derive(serde::Serialize, Deserialize)]
#[serde(default = "Jaime::default")]
struct Jaime
{
    /// Liste des choses à aimer
    liked : Vec<String>,
    /// Liste des manières de dire "J'aime"
    verbs : Vec<String>
}

/// Proverbes enregistrés (commande `proverbe`)
#[derive(serde::Serialize, Deserialize)]
#[serde(default = "Proverbs::default")]
struct Proverbs
{
    /// Liste des proverbes
    proverbs : Vec<String>
}


/* ==================== Variables images en RAM ==================== */
lazy_static::lazy_static! // Permet d'avoir des statics sans initialisation constante
{
    /// Contient la configuration globale, image en RAM du fichier JSON de config
    static ref CONFIGURATION : Mutex<Configuration> = Mutex::new(Configuration::default());

    /// Contient les données de la commande "aime", image en RAM de son fichier JSON
    static ref JAIME : Mutex<Jaime> = Mutex::new(Jaime::default());

    /// Liste de proverbes enregistrés
    static ref PROVERBS : Mutex<Proverbs> = Mutex::new(Proverbs::default());
}

/// Renvoie le chemin vers le fichier JSON de config, en se basant
/// sur la variable d'environement `BOTSCORD_DATA_PATH`
fn data_path() -> String
{
    return env::var("BOTSCORD_DATA_PATH").unwrap_or(String::from("/data"));
}


/* ==================== Valeurs par défaut des données persistentes ==================== */
impl Default for Configuration
{
    fn default() -> Configuration
    {
        return Configuration
        {
            prefix : '$',
            jaime_frequency : 0.1,
            proverb_frequency : 0.1
        };
    }
}

impl Default for Jaime
{
    fn default() -> Jaime
    {
        return Jaime
        {
            liked : vec![String::from("le bruit blanc de l'eau")],
            verbs : vec![String::from("J'aime")]
        };
    }
}

impl Default for Proverbs
{
    fn default() -> Proverbs
    {
        return Proverbs
        {
            proverbs : vec![String::from("Qui pisse face au vent se rince les dents.")],
        };
    }
}


/// À appeler une fois, avant tout autre appel du module.
/// Crée des squelettes de configuration (fichiers JSON avec valeurs par défaut)
/// puis charge la config en mémoire.
pub fn init() -> Result<(), String>
{
    if let Err(e) = save(".skeleton")
    {
        return Err(format!("Skeleton files creation failed : {}", e));
    }

    if let Err(e) = load()
    {
        return Err(format!("NVM load failed : {}", e));
    }

    return Ok(());
}

/// Charge toutes les données persitentes en mémoire
/// Renvoie OK si tout va bien, ou une erreur avec texte explicatif.
/// Si le **contenu** d'un fichier est invalide, renvoie une erreur, mais
/// si un fichier n'existe pas, renvoie OK et se contente de ne pas le charger.
pub fn load() -> Result<(), String>
{
    // 1- Configuration
    let path = data_path() + "/config.json";
    if Path::new(&path).exists()
    {
        match load_json(&path)
        {
            Ok(config_json) =>
            {
                if let Err(e) = load_config(config_json)
                {
                    return Err(String::from("Cannot load config : ") + &e);
                }
            },
            Err(e) => {return Err(String::from("Cannot load config : ") + &e);}
        }
        debug!("Loaded config from {}", path);
    }
    else
    {
        warn!("File {} not found, using default values instead", path);
    }

    // 2- Listes de "J'aime"
    let path = data_path() + "/jaime.json";
    if Path::new(&path).exists()
    {
        match load_json(&path)
        {
            Ok(jaime_json) =>
            {
                if let Err(e) = load_jaime(jaime_json)
                {
                    return Err(String::from("Cannot load JAIME lists : ") + &e);
                }
            },
            Err(e) => {return Err(String::from("Cannot load JAIME lists : ") + &e);}
        }
        debug!("Loaded JAIME lists from {}", path);
    }
    else
    {
        warn!("File {} not found, using default values instead", path);
    }

    // 3- Liste de proverbes
    let path = data_path() + "/proverbs.json";
    if Path::new(&path).exists()
    {
        match load_json(&path)
        {
            Ok(proverbs_json) =>
            {
                if let Err(e) = load_proverbs(proverbs_json)
                {
                    return Err(String::from("Cannot load proverbs list : ") + &e);
                }
            },
            Err(e) => {return Err(String::from("Cannot load proverbs list : ") + &e);}
        }
        debug!("Loaded proverbs list from {}", path);
    }
    else
    {
        warn!("File {} not found, using default values instead", path);
    }


    return Ok(());
}

/// Charge le fichier JSON et le renvoie sur forme d'objet JSON.
///
/// # Arguments
///
/// * `path` - Chemin du fichier JSON à charger
fn load_json(path : &str) -> Result<serde_json::Value, String>
{
    match fs::read_to_string(path)
    {
        Ok(text) =>
        {
            if let Ok(parsed) = serde_json::from_str(&text)
            {
                debug!("Loaded file {}", path);
                debug!("Content :\n{}", parsed);
                return Ok(parsed);
            }
            else
            {
                return Err(format!("Malformed json file {}", path));
            }
        }
        Err(error) =>
        {
            return Err(format!("Cannot load file {} : ", path) + &error.to_string());
        }
    }
}

/// Sauvegarde toutes les données persistentes dans des fichiers JSON
/// Renvoie OK si tout va bien, ou une erreur avec texte explicatif.
///
/// # Arguments
///
/// * `suffix` - Suffixe à ajouter aux noms des fichiers à écrite
///              (exemple : ".skeleton", ou "" pour pas de suffixe)
pub fn save(suffix : &str) -> Result<(), String>
{
    // 1- Configuration
    let path = data_path() + "/config.json" + suffix;
    let json_config = get_full_config()?;
    if let Err(e) = fs::write(&path, json_config + "\n")
    {
        return Err(format!("Failed to save configuration: {}", e));
    }
    debug!("Saved configuration to {}", &path);

    // 2- Liste de "j'aime"
    let path = data_path() + "/jaime.json" + suffix;
    let json_jaime = get_full_jaime()?;
    if let Err(e) = fs::write(&path, json_jaime + "\n")
    {
        return Err(format!("Failed to save JAIME lists: {}", e));
    }
    debug!("Saved JAIME lists to {}", &path);

    // 3- Liste de proverbes
    let path = data_path() + "/proverbs.json" + suffix;
    let json_proverbs = get_full_proverbs()?;
    if let Err(e) = fs::write(&path, json_proverbs + "\n")
    {
        return Err(format!("Failed to save proverbs list: {}", e));
    }
    debug!("Saved proverbs list to {}", &path);

    return Ok(());
}

/// Renvoie le préfixe configuré (un seul caractère)
/// Ne renvoie pas d'erreur, mais peut terminer le programme en affichant une erreur dans les logs
pub fn get_prefix() -> char
{
    if let Ok(config) = CONFIGURATION.lock()
    {
        return config.prefix;
    }
    else
    {
        error!("Cannot lock the global configuration mutex");
        exit(1);
    }
}


/// Change le préfixe dans la config (en RAM)
/// Renvoie OK ou ERREUR, sans plus de détails.
pub fn set_prefix(prefix : char) -> Result<(), ()>
{
    if let Ok(mut config) = CONFIGURATION.lock()
    {
        if prefix.to_string().len() == 1
        {
            config.prefix = prefix;
        }
        else
        {
            return Err(());
        }
    }
    else
    {
        error!("Cannot lock the global configuration mutex");
        return Err(());
    }
    return Ok(());
}


/// Renvoie la fréquence configurée pour les "j'aime".
/// Ne renvoie pas d'erreur, mais peut terminer le programme en affichant une erreur dans les logs
pub fn get_jaime_frequency() -> f32
{
    if let Ok(config) = CONFIGURATION.lock()
    {
        return config.jaime_frequency;
    }
    else
    {
        error!("Cannot lock the global configuration mutex");
        exit(1);
    }
}


/// Renvoie la fréquence configurée pour les "j'aime".
/// Ne renvoie pas d'erreur, mais peut terminer le programme en affichant une erreur dans les logs
pub fn get_proverb_frequency() -> f32
{
    if let Ok(config) = CONFIGURATION.lock()
    {
        return config.proverb_frequency;
    }
    else
    {
        error!("Cannot lock the global configuration mutex");
        exit(1);
    }
}


/// Change la fréquence des "j'aime" dans la config (en RAM)
/// Renvoie OK ou ERREUR, sans plus de détails.
pub fn set_jaime_frequency(frequency : f32) -> Result<(), ()>
{
    if let Ok(mut config) = CONFIGURATION.lock()
    {
        if frequency >= 0.0 && frequency <= 1.0
        {
            config.jaime_frequency = frequency;
        }
        else
        {
            error!("Frequency must be between 0 and 1");
            return Err(());
        }
    }
    else
    {
        error!("Cannot lock the global configuration mutex");
        return Err(());
    }
    return Ok(());
}


/// Change la fréquence des proverbes dans la config (en RAM)
/// Renvoie OK ou ERREUR, sans plus de détails.
pub fn set_proverb_frequency(frequency : f32) -> Result<(), ()>
{
    if let Ok(mut config) = CONFIGURATION.lock()
    {
        if frequency >= 0.0 && frequency <= 1.0
        {
            config.proverb_frequency = frequency;
        }
        else
        {
            error!("Frequency must be between 0 and 1");
            return Err(());
        }
    }
    else
    {
        error!("Cannot lock the global configuration mutex");
        return Err(());
    }
    return Ok(());
}


/// Renvoie un élément au hasard de la liste des choses aimées pour les "j'aime".
/// Ne renvoie pas d'erreur, mais peut terminer le programme en affichant une erreur dans les logs
pub fn get_random_liked() -> String
{
    if let Ok(jaime) = JAIME.lock()
    {
        // Safe car la liste contient au moins un élément dès l'initialisation
        return jaime.liked.choose(&mut rand::thread_rng()).unwrap().to_owned();
    }
    else
    {
        error!("Cannot lock the JAIME lists mutex");
        exit(1);
    }
}


/// Renvoie un élément au hasard de la liste des proverbes.
/// Ne renvoie pas d'erreur, mais peut terminer le programme en affichant une erreur dans les logs
pub fn get_random_proverb() -> String
{
    if let Ok(proverbs) = PROVERBS.lock()
    {
        // Safe car la liste contient au moins un élément dès l'initialisation
        return proverbs.proverbs.choose(&mut rand::thread_rng()).unwrap().to_owned();
    }
    else
    {
        error!("Cannot lock the proverbs list mutex");
        exit(1);
    }
}


/// Ajoute une phrase à la liste des choses aimées pour les "j'aime".
/// Renvoie une erreur en cas de problème interne, ne fait rien si elle y est déjà
///
/// # Arguments
///
/// * `sentence` - Phrase à ajouter à la liste
///
/// Returns
///
/// `Ok(())` si tout va bien, un texte d'erreur sinon
pub fn add_liked(sentence : String) -> Result<(), String>
{
    if let Ok(mut jaime) = JAIME.lock()
    {
        if ! jaime.liked.contains(&sentence)
        {
            debug!("Added sentence {} in liked list", sentence);
            jaime.liked.push(sentence);
        }
        else
        {
            warn!("Sentence '{}' already in liked list", sentence);
        }
    }
    else
    {
        return Err(String::from("Cannot lock the JAIME lists mutex"));
    }
    return Ok(());
}


/// Ajoute une phrase à la liste des proverbes.
/// Renvoie une erreur en cas de problème interne, ne fait rien si elle y est déjà
///
/// # Arguments
///
/// * `proverb` - Phrase à ajouter à la liste
///
/// Returns
///
/// `Ok(())` si tout va bien, un texte d'erreur sinon
pub fn add_proverb(proverb : String) -> Result<(), String>
{
    if let Ok(mut proverbs) = PROVERBS.lock()
    {
        if ! proverbs.proverbs.contains(&proverb)
        {
            debug!("Added sentence {} in proverbs list", proverb);
            proverbs.proverbs.push(proverb);
        }
        else
        {
            warn!("Sentence '{}' already in proverbs list", proverb);
        }
    }
    else
    {
        return Err(String::from("Cannot lock the proverbs list mutex"));
    }
    return Ok(());
}


/// Renvoie un élément au hasard de la liste des verbes (namiènes de dire "j'aime") pour les "j'aime".
/// Ne renvoie pas d'erreur, mais peut terminer le programme en affichant une erreur dans les logs
pub fn get_random_like_verb() -> String
{
    if let Ok(jaime) = JAIME.lock()
    {
        // Safe car la liste contient au moins un élément dès l'initialisation
        return jaime.verbs.choose(&mut rand::thread_rng()).unwrap().to_owned();
    }
    else
    {
        error!("Cannot lock the JAIME lists mutex");
        exit(1);
    }
}


/// Ajoute une phrase à la liste des manières de dire "j'aime".
/// Renvoie une erreur si elle y est déjà, ou en cas de problème interne.
pub fn add_like_verb(sentence : String) -> Result<(), String>
{
    if let Ok(mut jaime) = JAIME.lock()
    {
        if ! jaime.verbs.contains(&sentence)
        {
            debug!("Added sentence {} in like verbs list", sentence);
            jaime.verbs.push(sentence);
        }
        else
        {
            warn!("Sentence '{}' already in like verbs list", sentence);
        }
    }
    else
    {
        return Err(String::from("Cannot lock the JAIME lists mutex"));
    }
    return Ok(());
}


/// Écrit dans la configuration globale ce qui se trouve dans l'objet JSON donné.
/// Si un champ est manquant dans l'objet JSON, la valeur par défaut est utilisée.
/// Si il y a un champ en trop dans l'objet JSON, il est ignoré.
/// Si un champ existe mais a une valeur invalide, renvoie une erreur.
/// Revoie aussi une erreur si le mutex de config n'est pas accessible.
///
/// # Arguments
///
/// * `config` - L'objet JSON à utiliser
///
/// # Returns
///
/// * `OK(())` si la configuration a été écrite
/// * `Err("Texte d'explication")` sinon
fn load_config(config : serde_json::Value) -> Result<(), String>
{
    if let Ok(mut global_config) = CONFIGURATION.lock()
    {
        match serde_json::from_value::<Configuration>(config)
        {
            Ok(result) => {*global_config = result;}
            Err(e) => {return Err(e.to_string());}
        }

        if global_config.jaime_frequency < 0.0 || global_config.jaime_frequency > 1.0
        {
            return Err(String::from("'jaime_frequency' is not a frequency"));
        }
        if global_config.proverb_frequency < 0.0 || global_config.proverb_frequency > 1.0
        {
            return Err(String::from("'proverb_frequency' is not a frequency"));
        }
    }
    else
    {
        return Err(String::from("Cannot lock configuration mutex"));
    }

    return Ok(());
}


/// Écrit dans les listes de "J'aime" en RAM ce qui se trouve dans l'objet JSON donné.
/// Si il y a un champ en trop dans l'objet JSON, il est ignoré.
/// Si un champ existe mais a une valeur invalide, renvoie une erreur.
/// Revoie aussi une erreur si le mutex de config n'est pas accessible.
///
/// # Arguments
///
/// * `lists` - L'objet JSON à utiliser
///
/// # Returns
///
/// * `OK(())` si les listes ont été mises à jour
/// * `Err("Texte d'explication")` sinon
fn load_jaime(lists : serde_json::Value) -> Result<(), String>
{
    if let Ok(mut jaime) = JAIME.lock()
    {
        match serde_json::from_value::<Jaime>(lists)
        {
            Ok(result) => {*jaime = result},
            Err(e) => {return Err(e.to_string());}
        }
    }
    else
    {
        return Err(String::from("Cannot lock JAIME mutex"));
    }

    return Ok(());
}


/// Écrit dans la liste de proverbs en RAM ce qui se trouve dans l'objet JSON donné.
/// Si il y a un champ en trop dans l'objet JSON, il est ignoré.
/// Si un champ existe mais a une valeur invalide, renvoie une erreur.
/// Revoie aussi une erreur si le mutex de config n'est pas accessible.
///
/// # Arguments
///
/// * `list` - L'objet JSON à utiliser
///
/// # Returns
///
/// * `OK(())` si la liste & été mise à jour
/// * `Err("Texte d'explication")` sinon
fn load_proverbs(list : serde_json::Value) -> Result<(), String>
{
    if let Ok(mut proverbs) = PROVERBS.lock()
    {
        match serde_json::from_value::<Proverbs>(list)
        {
            Ok(result) => {*proverbs = result},
            Err(e) => {return Err(e.to_string());}
        }
    }
    else
    {
        return Err(String::from("Cannot lock JAIME mutex"));
    }

    return Ok(());
}


/// Revoie la configuration en entier (string JSON)
pub fn get_full_config() -> Result<String, String>
{
    if let Ok(config) = CONFIGURATION.lock()
    {
        if let Ok(text) = serde_json::to_string_pretty(&(*config))
        {
            return Ok(text);
        }
        else
        {
            return Err(String::from("Cannot serialize the config structure"));
        }
    }
    else
    {
        return Err(String::from("Cannot lock the global config mutex"));
    }
}


/// Revoie les listes de "J'aime" en entier (string JSON)
pub fn get_full_jaime() -> Result<String, String>
{
    if let Ok(jaime) = JAIME.lock()
    {
        if let Ok(text) = serde_json::to_string_pretty(&(*jaime))
        {
            return Ok(text);
        }
        else
        {
            return Err(String::from("Cannot serialize the JAIME lists"));
        }
    }
    else
    {
        return Err(String::from("Cannot lock the JAIME lists mutex"));
    }
}


/// Revoie la liste de proverbes en entier (string JSON)
pub fn get_full_proverbs() -> Result<String, String>
{
    if let Ok(proverbs) = PROVERBS.lock()
    {
        if let Ok(text) = serde_json::to_string_pretty(&(*proverbs))
        {
            return Ok(text);
        }
        else
        {
            return Err(String::from("Cannot serialize the proverbs list"));
        }
    }
    else
    {
        return Err(String::from("Cannot lock the proverbs list mutex"));
    }
}
