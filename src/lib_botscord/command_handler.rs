use std::env;
use super::nvm;

use super::llm;
use log::{error, warn, info};

use serenity::model::channel::Message;

/// Exécute la commande donnée.
///
/// # Arguments
///
/// * `msg` - Le message (entier) contenant la commande
///
/// # Returns
///
/// `Some(<réponse)` si il y a un message à envoyer en réponse, ou `None`.
/// En cas d'erreur la réponse sera un message d'erreur.
pub fn handle_command(msg: &Message) -> Option<String>
{
    info!("Incoming command : '{}'", msg.content);
    if let Some(command) = msg.content.get(1..)
    {
        if match_command(&command, "ping")
        {
            return Some(String::from("PONG"));
        }
        else if match_command(command, "mistral")
        {
            if let Some(prompt) = command.get(8..)
            {
                return command_mistral(prompt);
            }
            else
            {
                return Some(String::from("Erreur : suite de la commande manquante !"));
            }
        }
        else if match_command(command, "config")
        {
            return command_config(&command);
        }
        else if match_command(command, "aime")
        {
            if let Some(liked) = command.get(5..)
            {
                return command_aime(liked);
            }
            else
            {
                return Some(String::from("Erreur : suite de la commande manquante !"));
            }
        }
        else if match_command(command, "phrase_jaime")
        {
            if let Some(verb) = command.get(13..)
            {
                return command_phrase_jaime(verb);
            }
            else
            {
                return Some(String::from("Erreur : suite de la commande manquante !"));
            }
        }
        else if match_command(command, "proverbe")
        {
            if let Some(proverb) = command.get(9..)
            {
                return command_proverbe(proverb);
            }
            else
            {
                return Some(String::from("Erreur : suite de la commande manquante !"));
            }
        }
        else
        {
            warn!("Received unknown command");
            return Some(String::from("Commande inconnue !"));
        }
    }
    else
    {
        warn!("Received empty command");
        return Some(String::from("Erreur !"));
    }
}


/// Renvoie `true` si `in_command` commence par `command_name`.
fn match_command(in_command : &str, command_name : &str) -> bool
{
    if let Some(command) = in_command.get(0..(command_name.len()))
    {
        return command == command_name;
    }
    else
    {
        return false;
    }
}


/// Gère la commande "mistral" : evoie la suite de la commande en tant que prompt
/// à un service de LLM, selon les variables d'environement, par HTTP POST
/// **au format "OpenAI"** (compatible au moins avec OpenAI et Ollama).
/// ```bash
/// BOTSCORD_LLM_URL=<URL vers le endpoint>
/// BOTSCORD_LLM_TOKEN=<token pour le endpoint si besoin>
/// ```
fn command_mistral(prompt : &str) -> Option<String>
{
    let token = env::var("BOTSCORD_LLM_TOKEN").unwrap_or(String::from("LOCAL_DEV"));
    let mut answer = String::from("Erreur interne !");
    if let Ok(url) = env::var("BOTSCORD_LLM_URL")
    {
        answer = llm::request_llm_http("You are a helpful assistant.", prompt, &token, &url);
    }
    else
    {
        error!("Missing environment variable 'BOTSCORD_LLM_URL'");
    }
    return Some(answer);
}


/// Traite la commande "config" et toutes ses sous commandes :
/// * get <clé> - Renvoie la valeur de la config pour cette clé
/// * set <clé> <valeur> - Change la config (en RAM) pour cette clé à la valeur donnée
/// * save - Sauvegarde la config en RAM dans le fichier JSON
/// * reload - Charge la config depuis le fichier JSON, écrase celle en RAM avec
///
/// # Arguments
///
/// * command - la commande entière, sans le préfixe
fn command_config(command : &str) -> Option<String>
{
    if let Some(arguments) = command.get(7..)
    {
        if match_command(arguments, "get")
        {
            return command_config_get(arguments);
        }
        else if match_command(arguments, "set")
        {
            return command_config_set(arguments);
        }
        else if match_command(arguments, "save")
        {
            if let Err(error) = nvm::save("")
            {
                error!("Cannot save configuration : {}", error);
                return Some(String::from("Erreur : impossible de sauvegarder la configuration !"));
            }
            return Some(String::from("Configuration sauvegardée."));
        }
        else if match_command(arguments, "reload")
        {
            if let Err(error) = nvm::load()
            {
                error!("Cannot load configuration : {}", error);
                return Some(String::from("Erreur : impossible de charger la configuration !"))
            }
            return Some(String::from("Configuration rechargée."))
        }
        else
        {
            warn!("'config' command with invalid subcommand");
            return Some(String::from("Erreur : sous-commande invalide !\nSous commandes possibles : \
                                      `get <clé>`, `set <clé> <valeur>`, `save`, `reload`"));
        }
    }
    else  // Pas de sous commande : afficher la config entière
    {
        if let Ok(config_string) = nvm::get_full_config()
        {
            return Some(config_string);
        }
        return Some(String::from("Erreur lors de la lecture de la configuration !"));
    }
}


/// Traite la sous-commande `get` de la commande `config`.
///
/// # Arguments
///
/// * argument - Paramètres passés à la commande config (sans le "config" mais avec le "get")
fn command_config_get(arguments : &str) -> Option<String>
{
    if let Some(key) = arguments.split_whitespace().nth(1)
    {
        match key
        {
            "prefix" =>
            {
                return Some(String::from(nvm::get_prefix()));
            }
            "jaime_frequency" =>
            {
                return Some(nvm::get_jaime_frequency().to_string());
            }
            "proverb_frequency" =>
            {
                return Some(nvm::get_proverb_frequency().to_string());
            }
            _ =>
            {
                warn!("'config get' command on unknown key");
                return Some(String::from("Erreur : cette configuration n'existe pas !"));
            }
        }
    }
    else
    {
        warn!("'config get' command without providing any key");
        return Some(String::from("Utilisation : `config get <clé>`"));
    }
}


/// Traite la sous-commande `set` de la commande `config`.
///
/// # Arguments
///
/// * argument - Paramètres passés à la commande set (sans le "config" mais avec le "set")
fn command_config_set(arguments : &str) -> Option<String>
{
    if let Some(key_value) = arguments.get(4..)
    {
        let mut iter = key_value.split_whitespace();
        if let Some(key) = iter.next()
        {
            if let Some(value) = iter.next()
            {
                match key
                {
                    "prefix" =>
                    {
                        if value.len() == 1
                        {
                            match nvm::set_prefix(value.as_bytes()[0] as char)
                            {
                                Ok(_) =>
                                {
                                    info!("Config '{}' changed to '{}'", key, value);
                                    return Some(String::from("OK"));
                                }
                                Err(_) =>
                                {
                                    return Some(String::from("Erreur interne !"))
                                }
                            }
                        }
                        warn!("'config set prefix' command with invalid value");
                        return Some(String::from("Erreur : le préfixe doit faire exactement un caractère \
                                                    (et pas d'accent ou autre fourberie) !"))
                    }
                    "jaime_frequency" =>
                    {
                        if let Ok(frequency) = value.parse::<f32>()
                        {
                            if frequency >= 0.0 && frequency <= 1.0
                            {
                                match nvm::set_jaime_frequency(frequency)
                                {
                                    Ok(_) =>
                                    {
                                        info!("Config '{}' changed to '{}'", key, value);
                                        return Some(String::from("OK"));
                                    }
                                    Err(_) =>
                                    {
                                        return Some(String::from("Erreur interne !"))
                                    }
                                }
                            }
                            else
                            {
                                return Some(String::from("Erreur : la fréquence doit être entre 0 et 1 !"))
                            }
                        }
                        warn!("'config set jaime_frequency' command with invalid value");
                        return Some(String::from("Erreur : la fréquence doit être un nombre !"))
                    }
                    "proverb_frequency" =>
                    {
                        if let Ok(frequency) = value.parse::<f32>()
                        {
                            if frequency >= 0.0 && frequency <= 1.0
                            {
                                match nvm::set_proverb_frequency(frequency)
                                {
                                    Ok(_) =>
                                    {
                                        info!("Config '{}' changed to '{}'", key, value);
                                        return Some(String::from("OK"));
                                    }
                                    Err(_) =>
                                    {
                                        return Some(String::from("Erreur interne !"))
                                    }
                                }
                            }
                            else
                            {
                                return Some(String::from("Erreur : la fréquence doit être entre 0 et 1 !"))
                            }
                        }
                        warn!("'config set proverb_frequency' command with invalid value");
                        return Some(String::from("Erreur : la fréquence doit être un nombre !"))
                    }
                    _ =>
                    {
                        warn!("'config set' command with invalid key");
                        return Some(String::from("Erreur : cette configuration n'existe pas !"));
                    }
                }
            }
            else
            {
                warn!("'config set' command without providing any value");
            }
        }
        else
        {
            warn!("'config set' command without providing any key");
        }
    }
    else
    {
        warn!("'config set' command without providing any key");
    }
    return Some(String::from("Utilisation : `config set <clé> <valeur>`"));
}


/// Enregistre la string reçue dans la liste des choses aimées, pour les "j'aime"
///
/// # Arguments
///
/// * `liked` - Le truc à aimer, exemple : "le bruit blanc de l'eau"
fn command_aime(liked : &str) -> Option<String>
{
    match nvm::add_liked(String::from(liked))
    {
        Ok(()) => {return Some(String::from("OK"));}
        Err(e) =>
        {
            error!("{}", e);
            return Some(String::from("Erreur interne !"));
        }
    }
}


/// Enregistre la string reçue dans la liste des proverbes
///
/// # Arguments
///
/// * `proverb` - Le proverbe à enregistrer, exemple : "Qui pisse face au vent se rince les dents."
fn command_proverbe(proverb : &str) -> Option<String>
{
    match nvm::add_proverb(String::from(proverb))
    {
        Ok(()) => {return Some(String::from("OK"));}
        Err(e) =>
        {
            error!("{}", e);
            return Some(String::from("Erreur interne !"));
        }
    }
}


/// Enregistre la string reçue dans la liste des phrases pour dire "J'aime"
///
/// # Arguments
///
/// * `verb` - La manière de dire "J'aime", exemple : "J'apprécie"
fn command_phrase_jaime(verb : &str) -> Option<String>
{
    match nvm::add_like_verb(String::from(verb))
    {
        Ok(()) => {return Some(String::from("OK"));}
        Err(e) =>
        {
            error!("{}", e);
            return Some(String::from("Erreur interne !"));
        }
    }
}
