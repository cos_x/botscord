FROM alpine:latest AS builder
RUN apk add --no-cache cargo openssl-dev pkgconfig
WORKDIR /build
COPY src/ ./src/
COPY Cargo.toml .
RUN cargo build --release

FROM alpine:latest AS final
RUN apk add --no-cache libgcc
COPY --from=builder /build/target/release/botscord /app/botscord

CMD ["/app/botscord"]
