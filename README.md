# Botscord

## Variables d'environement

```
BOTSCORD_DISCORD_TOKEN=<token>
BOTSCORD_LLM_URL=<URL complète vers l'API du LLM>
BOTSCORD_LLM_TOKEN=<token pour le LLM>
BOTSCORD_VERBOSE=[<voir dans "Log">]
BOTSCORD_DATA_PATH=[<dossier de stockage persistant (défaut "/data")>]
```

## Log

Le niveau de log par défaut est ` INFO` pour le programme lui même et `WARNING` pour les dépendances.
Le niveau pour le programme peut être passé à `TRACE` en mettant n'importe quelle valeur dans la variable
d'environement `BOTSCORD_VERBOSE`.

## Données persistantes

Les données (config etc.) sont stockées sous forme de fichiers JSON dans `BOTSCORD_DATA_PATH`.
Au démarrage du programme, des squelettes de configuration sont créés dans ce dossier.
Si un fichier JSON ne contient pas tous les champs possible, une valeur par défaut sera utilisée
pour ces champs, mais si il contient des champs invalides, le programme ne démarrera pas.

## Docker
```
docker buildx build -t boscord:version --network=host --no-cache . # Ou un truc du genre
docker run --env [...] --mount source=<volume>,target=/data botscord
```
