use std::env;
use std::process::exit;
use log::{error, info, debug, LevelFilter};
use env_logger;
use botscord::lib_botscord::{dispatcher::dispatch_message, nvm};
use serenity::async_trait;
use serenity::model::channel::Message;
use serenity::model::gateway::Ready;
use serenity::prelude::*;

struct Handler;

#[async_trait]
impl EventHandler for Handler
{
    /// Function appelée quand un message est envoyé quelque part.
    /// Transmet le message pour traitement puis envoie dans le même salon texte
    /// la réponse si elle existe.
    async fn message(&self, ctx: Context, msg: Message)
    {
        let ctx_clone = ctx.clone();
        let msg_clone = msg.clone();
        let task = tokio::task::spawn_blocking(move || dispatch_message(&ctx_clone, &msg_clone));
        if let Ok(return_value) = task.await
        {
            if let Some(answer) = return_value
            {
                debug!("Sending message : {}", answer);
                let length = answer.chars().count();
                for start in (0..length).step_by(2000)
                {
                    let end = if length > start + 2000 {start + 2000} else {length};
                    let answer_part = &answer.chars().collect::<Vec<char>>()[start..end];
                    match msg.channel_id.say(&ctx.http, String::from_iter(answer_part)).await
                    {
                        Ok(_) => (),
                        Err(e) => {error!("Error while sending discord message : {}", e);},
                    }
                }
            }
        }
        else
        {
            error!("'dispatch_message' thread failure")
        }
    }

    /// Function appelé quand la connexion est établie : juste un log.
    async fn ready(&self, _: Context, ready: Ready)
    {
        info!("{} is connected!", ready.user.name);
    }
}


/// Function `main` : se connecte et attend des messages.
#[tokio::main]
async fn main()
{
    let mut log_level = LevelFilter::Info;
    if let Ok(_) = env::var("BOTSCORD_VERBOSE")
    {
        log_level = LevelFilter::Trace;
    }
    env_logger::Builder::new()
    .filter(None, LevelFilter::Warn)
    .filter(Some("botscord"), log_level)
    .init();

    if let Err(error) = nvm::init()
    {
        error!("Cannot load persistent data : {}", error);
        exit(1);
    }

    if let Ok(token) = env::var("BOTSCORD_DISCORD_TOKEN")
    {
        let intents = GatewayIntents::GUILD_MESSAGES
            | GatewayIntents::DIRECT_MESSAGES
            | GatewayIntents::MESSAGE_CONTENT;

        if let Ok(mut client) = Client::builder(&token, intents).event_handler(Handler).await
        {
            if let Err(why) = client.start().await
            {
                error!("Client error: {why:?}");
                exit(1);
            }
            else
            {
                error!("Cannot build  Discord client");
                exit(1);
            }

        }
    }
    else
    {
        error!("Missing environment variable 'BOTSCORD_DISCORD_TOKEN'");
        exit(1);
    }

}
